const sampleInitialReq = { "response": [   
    {descriptor: "assets", amount: 199432.99}, 
    {descriptor: "liabilities", amount: 199432.99}   
]};
// -----------------
const sampleInitialReqSTRING = JSON.stringify(sampleInitialReq);



const customFieldReq = { "response": [   
    {descriptor: "Outstanding debt", amount: 333.93}   
]};
// -----------------
const customFieldReqSTRING = JSON.stringify(customFieldReq);

const customFieldReq2 = { "response": [   
    {descriptor: "Other amounts", amount: 499.93}   
]};
// -----------------
const customFieldReqSTRING2 = JSON.stringify(customFieldReq2);




/* 

const exampleData = { 
    id: 798798439,
    assets: 199432.32, 
    liabilities: 199432.32, 
    rows: [
        {descriptor: "Outstanding debt", amount: -3238.93, table: "liabilities"},{descriptor: "Coming payments", amount: 9338.93, table: "assets"},{descriptor: "vehicles", amount: 98448.93, table: "assets"}]
};


*/