// GLOBAL VARIABLES
let CURRENT_FILE = '';   // this stores the file that is currently being displayed and processed, so that it can be accessed from all function
let CURRENT_FORMAT = ''; // this stores the currently selected format, so that it can be accessed from all function
// ==============================
CURRENT_ENTRY_FIELD = '';

function serverReq(file, searchParams, format) {
    console.log("Hallo");
    let url = ''; 
    if(searchParams == 'assets') {
        url = 'http://wutingfeng.pythonanywhere.com/api/item/' + file;
        console.log("assets");
    }
    else {
        console.log("others");
        let search = searchParams.replace(/ /g, '%20'); // this replaces all spaces with %20 instead, which is required for GET request. The == / /g is the modifier to make a global replacement (see https://www.w3schools.com/jsref/jsref_replace.asp)
        url = 'http://wutingfeng.pythonanywhere.com/api/item/' + file + '/' + search;
        console.log(url);
    }
    const proxyurl = "https://cors-anywhere.herokuapp.com/"; // this workaround is to get around the WEIRD PROBLEM of ==No 'Access-Control-Allow-Origin' header is present== SOLUTION: https://stackoverflow.com/questions/43871637/no-access-control-allow-origin-header-is-present-on-the-requested-resource-whe/43881141#43881141
    url = proxyurl + url;

    jQuery.get(url, function(result) {

        if(searchParams == 'assets') { // this means I just opened a NEW file
            renderAssetsAndLiab(result); // render the obtained data to the UI
        }
        else { // if search parameter is anything else, I just requested a custom field, and not an initial request
            addCardAndRender(result.response[0], CURRENT_ENTRY_FIELD); // render the new custom field into the card //  .result[0] is needed because the response is an ARRAY OF OBJECTS
        }
        console.log('result');
    });



    // if(searchParams == 'assets') {
    //     let serverResponse = sampleInitialReqSTRING;
    //     serverResponse = JSON.parse(serverResponse);
    //     return serverResponse;
    // }
    // else if(searchParams == 'Outstanding debt') {
    //     let serverResponse = customFieldReqSTRING;
    //     serverResponse = JSON.parse(serverResponse);
    //     return serverResponse;
    // }
    // else {
    //     let serverResponse = customFieldReqSTRING2;
    //     serverResponse = JSON.parse(serverResponse);
    //     return serverResponse;
    // }


}

function openFileReq(fileName) {
    // reset the UI to get ready for a new file:
    $('#asset_rows .row:not(.d-none)').remove(); // removes all rows except the template row, which has the class .d-none
    $('#liab_rows .row:not(.d-none)').remove(); // removes all rows except the template row, which has the class .d-none
    //

    CURRENT_FILE = fileName;

    serverReq(fileName,'assets',CURRENT_FORMAT); // for the fileOpen request, it will always be those two that are being requested
}

function renderAssetsAndLiab(data) {
    // EXTRACT DATA
    let ASSETS_amount = data.response.filter((item) => {
        return (item.descriptor == "assets");
    })[0].amount; // filter for those objects within the array whose 'descriptor' is 'assets'. This way, wherever it is stored, the filter() function can find it. 
    let LIAB_amount = data.response.filter((item) => {
        return (item.descriptor == "liabilities");
    })[0].amount;

    // update left table-header
    $('#assets-card .amount').html("$" + ASSETS_amount);

    // update right table-header
    $('#liab-card .amount').html("$" + LIAB_amount);
}

function customFieldRequest(searchParams, entryField) { // entryField means whether the user enter a custom field in liabilities or assets
    CURRENT_ENTRY_FIELD = entryField; // I must set this GLOBAL variable here so that I can access it in the server callback
    serverReq(CURRENT_FILE, searchParams, CURRENT_FORMAT)

    $("#customAssetsField").val(''); // reset the entry field
    $("#customLiabsField").val(''); // reset the entry field
}

function addCardAndRender(data, assetsOrLiabs) {

    if(assetsOrLiabs == 'assets') {
        $('#ASSETS_custom_row_template .TEMPLATE_desc').html(data.descriptor);
        $('#ASSETS_custom_row_template .TEMPLATE_amount').html('$' + data.amount);

        let new_row = $('#ASSETS_custom_row_template').clone();
        new_row.removeClass('d-none');
        new_row.attr('id',''); // this is to prevent the same ID being used twice
        $('#asset_rows').append(new_row);
    }
    else {
        $('#LIABS_custom_row_template .TEMPLATE_desc').html(data.descriptor);
        $('#LIABS_custom_row_template .TEMPLATE_amount').html('$' + data.amount);

        let new_row = $('#LIABS_custom_row_template').clone();
        new_row.removeClass('d-none');
        new_row.attr('id',''); // this is to prevent the same ID being used twice
        $('#liab_rows').append(new_row);
    }
}

$(document).ready(function () {
    $('#file-list a').click(function () {
        openFileReq($(this).html());
    });
});

// =================
// =================
// =================